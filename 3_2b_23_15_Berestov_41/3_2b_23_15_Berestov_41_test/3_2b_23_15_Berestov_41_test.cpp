
#include "stdafx.h"
#include "..\3_2b_23_15_Berestov_41\3_2b_23_15_Sequence.h"
#include "gtest\gtest.h"

TEST(SequenceTesting, CheckException)
{
	int arr[] = { 0,-1,90 };
	Sequence S3 = Sequence(3, arr);
	ASSERT_ANY_THROW(Sequence(3));
	ASSERT_ANY_THROW(Sequence(3, arr));
	ASSERT_ANY_THROW(Sequence(S3));
}

TEST(SequenceTesting, CheckConstructor1)			
{
	Sequence S1 = Sequence();
	ASSERT_EQ(0, S1.getSize());
}

TEST(SequenceTesting, CheckConstructor2)
{
	Sequence S2 = Sequence(3);
	ASSERT_EQ(S2[0], 3);
}

TEST(SequenceTesting, CheckConstructor3)
{
	int arr[] = { 0,-1,90 };
	Sequence S3 = Sequence(3, arr);
	ASSERT_EQ(3, S3.getSize());
	for (int i = 0; i < 3; i++)
		ASSERT_EQ(arr[i], S3[i]);
	int g[1] = { 5 };
	Sequence S22 = Sequence(-1, g);
	ASSERT_EQ(0, S22.getSize());
}

TEST(SequenceTesting, CheckConstructor4)			
{
	int arr[] = { 0,-1,90 };
	Sequence S3 = Sequence(3, arr);
	Sequence S4 = Sequence(S3);
	ASSERT_EQ(3, S4.getSize());
	for (int i = 0; i < 3; i++)
		ASSERT_EQ(arr[i], S4[i]);
}

TEST(SequenceTesting, Check)
{
	int arr[] = { 0,-1,90 };
	Sequence S = Sequence(3, arr);
	ASSERT_ANY_THROW(S[4]);
	ASSERT_ANY_THROW(S[-1]);
}

TEST(SequenceTesting, CheckIO)					//���� ������
{
	int* arr = new int[3];
	arr[0] = 0;
	arr[1] = -1;
	arr[2] = 90;
	Sequence S3 = Sequence(3, arr);
	ostringstream ost0;
	S3.print(ost0);
	ASSERT_STREQ(ost0.str().c_str(), "0 -1 90 ");

	ostringstream ost;
	ost << Sequence(3);
	ASSERT_STREQ(ost.str().c_str(), "3 ");

}

TEST(SequenceTesting, CheckIndexing)			//���� ��������� []
{
	int* arr2 = new int[6];
	for (int i = 0; i < 6; i++)
		arr2[i] = 2 * i;
	Sequence S5 = Sequence(6, arr2);
	ASSERT_EQ(6, S5.getSize());
	for (int i = 0; i < 6; i++)
		ASSERT_EQ(arr2[i], S5[i]);
}

TEST(SequenceTesting, CheckAddition)			//���� ���������� + � ������� ��������
{
	Sequence S1 = Sequence(3);
	int* arr = new int[3];
	arr[0] = 0;
	arr[1] = -1;
	arr[2] = 90;
	Sequence S2 = Sequence(3, arr);
	Sequence S = Sequence();
	S = S2 + S1;
	ASSERT_EQ(4, S.getSize());
	ASSERT_EQ(0, S[0]);
	ASSERT_EQ(-1, S[1]);
	ASSERT_EQ(90, S[2]);
	ASSERT_EQ(3, S[3]);

	S += 9;
	ASSERT_EQ(5, S.getSize());
	ASSERT_EQ(9, S[4]);
	S.clear();

	S = -10 + S1;
	ASSERT_EQ(2, S.getSize());
	ASSERT_EQ(-10, S[0]);
	ASSERT_EQ(3, S[1]);

	S = S + 4;
	ASSERT_EQ(3, S.getSize());
	ASSERT_EQ(-10, S[0]);
	ASSERT_EQ(3, S[1]);
	ASSERT_EQ(4, S[2]);

	S = S1.addition(S2);
	ASSERT_EQ(4, S.getSize());
	ASSERT_EQ(3, S[0]);
	ASSERT_EQ(0, S[1]);
	ASSERT_EQ(-1, S[2]);
	ASSERT_EQ(90, S[3]);
	S.clear();

	S.add(2).add(3);
	ASSERT_EQ(2, S.getSize());
	ASSERT_EQ(2, S[0]);
	ASSERT_EQ(3, S[1]);
}

TEST(SequenceTesting, CheckCut)			//���� cut
{
	int* arr = new int[5];
	arr[0] = 0;
	arr[1] = -1;
	arr[2] = 90;
	arr[3] = 91;
	arr[4] = 100;
	Sequence S2 = Sequence(5, arr);
	Sequence S = S2.cut();
	ASSERT_EQ(4, S.getSize());
	ASSERT_EQ(-1, S[0]);
	ASSERT_EQ(90, S[1]);
	ASSERT_EQ(91, S[2]);
	ASSERT_EQ(100, S[3]);

	Sequence S3 = Sequence(3).cut();
	ASSERT_EQ(S3.getSize(), 0);
}

TEST(SequenceTesting, CheckCF)			//���� count � frequency
{
	int* arr = new int[5];
	arr[0] = 90;
	arr[1] = -1;
	arr[2] = 90;
	arr[3] = 90;
	arr[4] = 90;
	Sequence S2 = Sequence(5, arr);
	ASSERT_EQ(2, S2.count());
	ASSERT_EQ(4, S2.frequency(90));
	ASSERT_EQ(1, S2.frequency(-1));
	ASSERT_EQ(0, S2.frequency(0));

}

int _tmain(int argc, _TCHAR* argv[])
{
	int point;
	::testing::InitGoogleTest(&argc, argv);
	point = RUN_ALL_TESTS();
	system("pause");
	return point;
}