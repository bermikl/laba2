// Application.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "..\3_2b_23_15_Berestov_41\3_2b_23_15_Sequence.h"

#include <iostream>
#include <string>
#include <exception>

using namespace std;


string msgs[] = { "Exit", "Print menu", "Print sequences", "Input sequences" , "Summarise sequences" ,
"Cut sorting sequence", "Add number", "Get count of unique items" ,"Get frequency of number", "Get size",
"Get member","Clear sequence", "Change mode" };								//������ ����
const int amount = 13;						//���������� �������
static bool op;								//���� true �� ������������ ������������� ����������,
											//���� false �� ��������������� ������
Sequence* CreateNew();						//������� �������� ������������������� 3 ���������

int Exit(Sequence**);						//������� ������
int Menu(Sequence**);						//����
int Print(Sequence**);						//����� �������������������
int Input(Sequence**);						//���� �������������������
int Summarise(Sequence**);					//����� ��� �������������������
int Sorting(Sequence**);					//����� ������������� ���������������������
int Add(Sequence**);						//���������� ����� � ������������������
int Unique(Sequence**);						//����������� ���������� ���������� ���������
int Frequency(Sequence**);					//����������� ������� ��������� �����
int Size(Sequence**);						//��������� ������� ������������������
int Member(Sequence**);						//��������� ����� �� �������
int Clear(Sequence**);						//������� ������������������
int ChangeMode(Sequence**);					//���������� ����� ������������� �������. (�������� op).

int getInt();								//������� ���������� ������ �����
int getIndex();								//���������� ������ ����� �� 0 �� 2

int main()
{
	op = true;														//�������� ������������ � �����������
	Sequence** S;													//������ �� 3 �������������������
	try
	{
		S = new Sequence*[3];										//��� ������ ����� �������������� ��� ��������� ���� ��������
		for (int i = 0; i < 3; i++)
			S[i] = CreateNew();
	}
	catch (bad_alloc)												//�������� ����������, ���������� � ��������� ������
	{
		cout << "Out of memory. :-(" << endl;
		return 1;
	}
	int ex = 0;														//��������� ��� �������� ������ ����
	char choice = 0;												//����� �� ����� - ����� ����
	for (int i = 0; i<amount; ++i)
	{
		cout << (char)(i + 'a') << " " << msgs[i] << endl;
	}
	int(*func[amount]) (Sequence**) = { Exit, Menu, Print, Input, Summarise, Sorting, Add, Unique, Frequency, Size, Member, Clear,ChangeMode };					//������ �������

	while (!ex)
	{																//���� ������ ������
		cout << "\nMake your choice: ";
		cin >> choice;
		while (!((choice>('a' - 1)) && ((choice<amount + 'a'))))	//���� �� ����� ���������� ����� ������
			cin >> choice;
		if ((choice>('a' - 1)) && ((choice<amount + 'a')))
			ex = (*func[choice - 'a'])(S);							//������� ��������, ������� ������� ��������
	}

	return 0;
}

//������� �������� ����� �������������������. ������ 'a' ����� ������� � ������� ������� ������������, 'b' - ������� � ������� ������������ - � ����� ������, 'c' - ������ �� ��������� �����
Sequence* CreateNew()
{
	cout << "Enter a, to create sequence used empty constructor." << endl;
	cout << "Enter b, to use constructor with one number." << endl;
	cout << "Enter other key, to use constructor with any items." << endl;
	char c = '0';
	cin >> c;
	switch (c)
	{
	case 'a':														//������ � ������ �������������
		return new Sequence();
	case 'b':
	{
		cout << "Enter number" << endl;								//
		int number = getInt();										//���������� �����
		return new Sequence(number);								//����� ������������
	}
	default:
		cout << "Enter number of items" << endl;
		int length = getInt();										//���������� �����
		if (length > 0)												//�������� ������������
		{
			int* items = new int[length];							//������ ������ ���������
			for (int i = 0; i < length; i++)						//��������� ���
			{
				cout << "Enter " << i << " item: ";
				cin >> items[i];
			}
			return new Sequence(length, items);						//���������� ��������� �� ����� ������
		}
		else
			return new Sequence();									//��� ������������ ����� ������ ������ �������
	}
}

int Exit(Sequence** S) 												//���������� ���������
{
	for (int i = 0; i < 3; i++)
		delete S[i];												//����� ����������� ��� �������� ��������
	delete[] S;														//������� ������, ��������� ��� ���������� �� ������������������
	return 1;
}

int Menu(Sequence**)												//����� ����
{
	int i;
	printf("\n");
	for (i = 0; i < amount; ++i)
		cout << (char)(i + 'a') << " " << msgs[i] << ", ";
	return 0;
}

int Print(Sequence** S)												//����� �������������������
{
	if (op)															//����� � ������� ���������
		for (int i = 0; i < 3; i++)
			cout << i << " sequence: " << *S[i] << endl;
	else
		for (int i = 0; i < 3; i++)
		{
			cout << i << " sequence: ";								//����� � ������� ������
			S[i]->print(cout);
			cout << endl;
		}
	return 0;
}

int Input(Sequence** S)
{
	cout << "Enter '0' to add new elements to S0" << endl;
	cout << "Enter '1' to add new elements to S1" << endl;
	cout << "Enter '2' to add new elements to S2" << endl;
	int ind = getIndex();											//�������� ������������������ ��� �����
	cout << "Enter count of items and enter items" << endl;
	try 
	{
		if (op)
			cin >> *S[ind];												//���� � ������� ��������������� ���������
		else
			S[ind]->input();											//� ������� ������
	}
	catch (bad_alloc)
	{
		cout << "Out of memory." << endl;
		return 1;
	}
	return 0;
}

int Summarise(Sequence** S)											//�����������: � = �1 + �2, ������� ������ ����� ������������������-����������
{
	cout << "Choose sequence for result." << endl;
	cout << "Enter 0 to choose S0, 1 - S1, 2 - S2." << endl;		//
	int r = getIndex();												//���������
	cout << "Choose first summand." << endl;
	cout << "Enter 0 to choose S0, 1 - S1, 2 - S2." << endl;
	int a1 = getIndex();											//����� ����� ������� ��������
	cout << "Choose second summand." << endl;
	cout << "Enter 0 to choose S0, 1 - S1, 2 - S2." << endl;
	int a2 = getIndex();											//����� ������� ��������
	try 
	{
		if (op)
			*S[r] = *S[a1] + *S[a2];									//�������� � ������� ��������� +
		else
			*S[r] = S[a1]->addition(*S[a2]);							//�������� � ������� ������ ������
	}
	catch (bad_alloc)
	{
		cout << "Out of memory." << endl;
		return 1;
	}
	return 0;
}

int Sorting(Sequence** S)											//��������� ������������� ���������������������
{
	cout << "Choose sequence for result." << endl;
	cout << "Enter 0 to choose S0, 1 - S1, 2 - S2." << endl;
	int r = getIndex();												//���������� ����� ������������������, ���� ������� ���������
	cout << "Choose sequence for finding sorting sequence." << endl;
	cout << "Enter 0 to choose S0, 1 - S1, 2 - S2." << endl;
	int s = getIndex();												//�� ����� ������������������ ������� ���������������������
	try
	{
		*S[r] = S[s]->cut();										//�������� ���������������������
		cout << "New sequence: " << *S[r] << endl;
	}
	catch (bad_alloc)												//��������� ���������� - ��������� ������
	{
		cout << "Out of memory." << endl;
		return 1;
	}
	return 0;
}

int Add(Sequence** S)												//���������� ����� � �����������������
{
	cout << "Choose sequence for result." << endl;
	cout << "Enter 0 to choose S0, 1 - S1, 2 - S2." << endl;
	int r = getIndex();												//���������� ����� ������������������, ���� ������� �����
	cout << "Enter number." << endl;
	int n = getInt();												//������ �����
	try
	{
		if (op) {													//���������� ����������
																	//*S[r] += n;											//��������� ����� � ����� � ������� +=				
			*S[r] = *S[r] + n;										//��������� ����� � ������ �������
																	//*S[r] = n + *S[r];
		}
		else
			S[r]->add(n);											//��������� ����� � ����� ������������������ � ������� ������ 
		cout << "Number " << n << " added." << endl;				//
	}
	catch (bad_alloc)
	{
		cout << "Out of memory." << endl;
		return 1;
	}
	return 0;
}

int Unique(Sequence** S)											//���������� ���������� ���������� ���������
{
	for (int i = 0; i < 3; i++)
		cout << "S" << i << " contain " << S[i]->count() << " unique items." << endl;
	return 0;
}

int Frequency(Sequence** S)											//���������� ������� ��������� ��������
{
	int n = 0;
	cout << "Enter number." << endl;
	cin >> n;														//���� ��������� �����
	for (int i = 0; i < 3; i++)
		cout << "S" << i << " contain " << S[i]->frequency(n) << " number " << n << '.' << endl; //���������� ��� ������ ������������������
	return 0;
}

int Size(Sequence** S)												//��������� ������� ������������������
{
	for (int i = 0; i < 3; i++)
		cout << "S" << i << " size = " << S[i]->getSize() << endl; //���������� ��� ������ ������������������
	return 0;
}
int Member(Sequence** S)				//��������� ����� �� �������
{
	cout << "Choose sequence." << endl;
	cout << "Enter 0 to choose S0, 1 - S1, 2 - S2." << endl;
	int n = getIndex();												//���������� ����� ������������������, ���� ������� �����
	cout << "Enter index." << endl;
	int i = getInt();												//������ �����
	try
	{
		cout << "i = " << i << " : " << (*S[n])[i] << endl;
	}
	catch (exception e)
	{
		cout << e.what() << endl;
	}
	return 0;
}

int Clear(Sequence** S) {				//������� ������������������
	cout << "Choose sequence." << endl;
	cout << "Enter 0 to choose S0, 1 - S1, 2 - S2." << endl;
	int n = getIndex();												//���������� ����� ������������������, ���� ������� �����
	S[n]->clear();
	return 0;
}

int ChangeMode(Sequence** S)
{
	cout << "Enter 'o' to use operators, else - use methods" << endl;
	char c = 'f';
	cin >> c;
	op = c == 'o';
	return 0;
}

int getInt()														//��������� ������ �����, 
{
	int n = 0;
	do																//���� �� ����� ������ ����������
	{
		cin.clear();												//������� ������ 
		cin.ignore();												//������������� ������
		cin >> n;
	} while (!cin.good());											//���� ����� �� good(), �� ���������� ��������, ������� ����� �����, ��������������� �����
	return n;
}

int getIndex()														//��������� 0 ��� 1 ��� 2 (����� ������������������ � �������
{
	int i = -1;
	while (i < 0 || i > 2)											//��������� ����� �����, ���� ��� �� �������� � ���������� �� 0 �� 2
		i = getInt();
	return i;
}