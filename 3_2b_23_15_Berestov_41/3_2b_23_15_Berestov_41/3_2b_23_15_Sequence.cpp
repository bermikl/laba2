#include "stdafx.h"
#include "3_2b_23_15_Sequence.h"


Sequence::Sequence()									//������ �����������
{
	size = 0;
	members = new int[0];
	//cout << "C1";
}

Sequence::Sequence(int member)							//����������� � ����� ������
{
	size = 1;
	members = new int[1];
	members[0] = member;
	//cout << "C2";
}


Sequence::Sequence(int length, const int* newMembers)	//����������� �� ���������� �����
{
	length = length < 0 ? length = 0 : length;
	members = new int[length];
	for (size = 0; size < length; size++)
		members[size] = newMembers[size];					
	//cout << "C3";
}

Sequence::Sequence(Sequence&& S)						//������������ �����������
{
	members = S.members;
	size = S.size;
	S.size = 0;
	S.members = NULL;
	//cout << "P2";
}

Sequence::Sequence(const Sequence& S)					//���������� ����������
{
	members = new int[S.size];
	for (size = 0; size < S.size; size++)
		members[size] = S.members[size];
	//cout << "P1";
}

Sequence::~Sequence()									//����������
{
	delete[]members;
	//cout << "D";
}

Sequence& Sequence::operator= (const Sequence& S)		//���������� �������� ������������
{
	if (this != &S)
	{
		members = new int[S.size];
		for (size = 0; size < S.size; size++)
			members[size] = S.members[size];
	}
	//cout << "=1";
	return *this;
}

Sequence& Sequence::operator= (Sequence&& S)			//������������ �������� ������������
{
	if (this != &S)
	{
		members = S.members;
		size = S.size;
		S.size = 0;
		S.members = nullptr;
	}
	//cout << "=2";
	return *this;
}

void Sequence::clear()
{
	delete[] members;
	size = 0;
	members = new int[0];
}


void Sequence::input()									//����� �����
{
	int amount;
	cin >> amount;										
	if (amount > 0)
	{
		int * m = new int[size + amount];
		for (int i = 0; i < size; i++)
			m[i] = members[i];
		for (int i = 0; i < amount; i++, size++)
			cin >> m[size];							
		delete[] members;
		members = m;
	}
}

istream& operator >>(istream& c, Sequence& S)			//����������� ����
{
	int s = 0;
	c >> s;												
	if (c.good())										
		if (s > 0)										
			for (int i = 0; i < s && c.good(); i++)		
			{
				int k = 0;								
				c >> k;
				S += k;									
			}
		else
			c.setstate(ios::failbit);					
	else;
	return c;
}

ostream& Sequence::print(ostream& s)
{
	for (int i = 0; i < size; i++)
		s << members[i] << ' ';
	return s;
}


ostream& operator<< (ostream& s, const Sequence& S)		//���������� ��������� ������
{
	for (int i = 0; i < S.size; i++)
		s << (S.members)[i] << ' ';
	return s;
}

const int Sequence::operator[] (int i) const
{
	if (i < 0 || i >= size)
		throw exception("Out of range.");
	return members[i];
}

Sequence Sequence::addition(const Sequence& S) const	//����� ��������
{
	Sequence NewS = Sequence();							
	NewS.members = new int[size + S.size];
	for (NewS.size = 0; NewS.size < size; NewS.size++)	
		NewS.members[NewS.size] = members[NewS.size];
	for (; NewS.size < S.size + size; NewS.size++)	    
		NewS.members[NewS.size] = S.members[NewS.size - size];
	return NewS;									
}

Sequence Sequence::operator+ (const Sequence & S) const	//���������� ��������� Sequence + Sequence �, �������������, Sequence + int
{
	Sequence NewS = Sequence();							
	NewS.members = new int[size + S.size];
	for (NewS.size = 0; NewS.size < size; NewS.size++)	
		NewS.members[NewS.size] = members[NewS.size];
	for (; NewS.size < S.size + size; NewS.size++)	
		NewS.members[NewS.size] = S.members[NewS.size - size];
	return NewS;
}

Sequence Sequence::operator+ (int n) const
{
	Sequence NewS = Sequence();
	NewS.members = new int[size + 1];
	for (NewS.size = 0; NewS.size < size; NewS.size++)	//�������� ������ ��� �������� �������� ����� ��������
		NewS.members[NewS.size] = members[NewS.size];
	NewS.members[NewS.size++] = n;
	return NewS;

}

const Sequence operator+ (int n, const Sequence& S)		//���������� ��������� int + Sequence
{
	Sequence NewS = Sequence(n);						
	return NewS + S;									
}




Sequence Sequence::cut() const							//�������� ����� ������������������ �� ������ ������������� ���������������������
{
	int length = 0;										
	int ind = 0;										
	for (int i = 0; i <= size - 3 && length < 3; i++)	
	{
		int k = i;										
		for (k = i; members[k + 1] >= members[k]; k++);	
		if (length < k - i + 1)
		{
			length = k - i + 1;
			ind = i;
		}
		for (k = i; members[k + 1] <= members[k]; k++);	
		if (length < k - i + 1)
		{
			length = k - i + 1;
			ind = i;
		}
	}
	if (length < 3)										//���� ������������������ ������ ������ 3, �� ����� ����� 0
		length = 0;
	return Sequence(length, members + ind);				//���������� ����� ������������������
}

Sequence& Sequence::add(int n)							//��������� � ������������������ �����
{
	int* m = new int[size + 1];
	for (int i = 0; i < size; i++)
		m[i] = members[i];
	m[size] = n;
	size++;
	delete[] members;
	members = m;
	return *this;									
}

Sequence& Sequence::operator+= (int n)					//���������� ��������� += 
{
	int* m = new int[size + 1];
	for (int i = 0; i < size; i++)
		m[i] = members[i];
	m[size] = n;
	size++;
	delete[] members;
	members = m;
	return *this;									
}

int Sequence::count() const								//���������� ����� ���������� ��������
{
	int count = 0;
	bool exist = false;									
	for (int i = 0; i < size; i++)						
	{
		exist = false;
		for (int k = 0; k < i; k++)
			if (members[i] == members[k])
				exist = true;
		if (!exist)										
			count++;
	}
	return count;
}

int Sequence::frequency(int n) const					//���������� ������� ��������� ����� ���� ������� �� �������
{
	int k = 0;
	for (int i = 0; i < size; i++)
		if (members[i] == n)
			k++;
	return k;
}







