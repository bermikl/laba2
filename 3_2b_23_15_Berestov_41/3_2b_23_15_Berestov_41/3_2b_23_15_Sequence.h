#pragma once
#include <iostream>
#include <sstream>

using namespace std;

class Sequence
{
private:
	int size;														//������� ���������� ��������� � ������������������
	int* members;													//������ ��������� ������������������


public:
	Sequence();														//������ �����������
	Sequence(int m);												//����������� �� ������ �����
	Sequence(int size, const int* m);								//����������� �� ���������� �������� ������
	Sequence(const Sequence& S);									//���������� �����������
	Sequence(Sequence&&);											//������������ �����������
	~Sequence();													//����������
	void clear();													//����� �������� ���� ��������� �������

	Sequence& operator= (const Sequence& S);						//���������� �������� ������������
	Sequence& operator= (Sequence&& S);								//������������ �������� ������������

	void input();													//����� �����
	friend istream& operator >>(istream&, Sequence&);				//������������� ������������� �������� �����

	ostream& print(ostream&);
	friend ostream& operator <<(ostream&, const Sequence&);			//������������� ������������ �������� ������ ����� (�����)

	int getSize() const { return size; }							//��������� �������
	const int operator[] (int) const;

	Sequence cut() const;											//��������� ��������������� ���������������������

	Sequence addition(const Sequence& S) const;						//����� �������� ���� ����������������������
	Sequence operator+ (const Sequence& S) const;					//�������� �������� ���� �������������������	
	Sequence operator+ (int) const;									//�������� Sequence + int

	friend const Sequence operator+(int, const Sequence& S);		//�������� �������� ����: int + Sequence

	Sequence& add(int n);											//����� ���������� ����� � ����� ������������������
	Sequence& operator+= (int n);									//��������: Sequence += int

	int	count() const;												//��������� ����� ���������� ���������
	int	frequency(int n) const;										//��������� ������� ��������� ����� � ������������������

};

